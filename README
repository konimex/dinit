This is an experimental repo for dinit on Artix.

If my tests show positive results, an ISO will be published here.

========================================================================

Directory and Configuration Structure
=====================================

dinit's main directory is /etc/dinit.d/, with all services residing
there. Configuration files can be added per service in
/etc/dinit.d/config. All *enabled* service will be linked in
/etc/dinit.d/boot.d so it will be started on boot. Dinit will also look
into /lib/dinit.d and /usr/local/lib/dinit.d for services.

Some scripted services (such as cryptsetup) will need to be stored
somewhere, /etc/dinit.d/scripts will be the place for the scripts
(mostly shell).

All services will be put into /etc/dinit.d, similar to OpenRC's
/etc/init.d.

Controlling dinit services
==========================

dinitctl(8) is the way to go to control dinit services. However, it's
reload works quite differently than the usual SysV reload.


General syntax
==============

dinit's general syntax configuration for standalone services look like
this:

  type       = process | bgprocess | scripted
  command    = /needs/an/absolute/path/to/bin
  logfile    = logfile
  depends-on = another-service
  waits-for  = yet-another-services
  options    = no-sigterm | runs-on-console | starts-on-console | start-interruptible

More properties can be seen in dinit-service(5).


Dependency types
================

There are three types of dependencies in dinit, depends-on, depends-ms,
and waits-for.

depends-on are hard dependencies, or, the named service must be run
first before running the service which depends on it, otherwise the
service will fail to start. If the named service is stopped, then the
service which depends on the named service will also be stopped.

depends-ms are "milestone dependencies", it is similar with hard
dependencies, but if the named service is stopped, the service which
depends on it will not be stopped.

waits-for are soft dependencies, so the named service can fail to start,
but the service which depends on it can still start.


Oneshots
========

For oneshot services, we can use scripted, it has additional syntax,
stop-command. Otherwise, it's almost the same as the usual general
syntax.


Background
==========

Fortunately, dinit also supports background process, by using the
bgprocess type. We will need to specify the pid-file though.


Conclusion
==========

TBA
